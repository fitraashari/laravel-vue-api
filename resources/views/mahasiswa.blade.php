<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Daftar Kehadiran Mahasiswa</title>
  </head>
  <body>
    <div class="container ">
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="card shadow rounded" >
                    <div class="card-header bg-secondary text-light">
                        <h4>Daftar Kehadiran Mahasiswa</h4>
                    </div>
                    <div class="card-body">
                        <div id="app">
                        
                            <div class="form-group">
                              <label for="nim">NIM</label>
                              <input type="text" class="form-control" id="nim" v-model="nim" required>
                            </div>
                            <div class="form-group">
                              <label for="nama">Nama</label>
                              <input type="text" class="form-control" id="nama" v-model="nama" required>
                            </div>
                            <div class="form-group">
                              <label for="jenis_kelamin">Jenis Kelamin</label>
                              {{-- <input type="text" class="form-control" id="jenis_kelamin" v-model="jenis_kelamin"> --}}
                              <select v-model="jenis_kelamin" class="form-control" id="jenis_kelamin" required>
                                  <option disabled='' selected>---Jenkel---</option>
                                  <option value="L">Laki Laki</option>
                                  <option value="P">Perempuan</option>
                              </select>
                            </div>
                            <button class="btn btn-primary" v-on:click="addMhs">Submit</button>
                         
            <table class="table table-bordered mt-2 shadow border border-info">
                <thead class="bg-info text-light">
                          <tr>
                          <th>No</th>
                          <th>NIM</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>Status Kehadiran</th>
                          <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-For="(mhs,index) in mahasiswa">
                        <td>@{{index+1}}</td>
                        <td>@{{mhs.nim}}</td>
                        <td>@{{mhs.nama}}</td>
                        <td>@{{mhs.jenis_kelamin}}</td>
                        <td v-if='mhs.status_kehadiran' class="text-success">Hadir</td>
                        <td v-else class="text-warning">Belum Hadir</td>
                        <td>
                            <button  v-if='mhs.status_kehadiran' v-on:click="cekHadir(index,mhs)" class="btn btn-sm btn-warning"><i class="fa fa-times"></i></button>
                            <button v-else v-on:click="cekHadir(index,mhs)" class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>
                            <button v-on:click="deleteMhs(index,mhs)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                        
                        </td>
                           
                        </tr>
                    </tbody>    
                    </table>
                          </div>
                    </div>
                    <div class="card-footer text-muted">
                      <small>Fitra ashari</small>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> --}}
    <!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

<script>
    new Vue({
        el:'#app',
        data:{
            id:0,
            nim:0,
            nama:'',
            jenis_kelamin:'',
            status_kehadiran:0,
            mahasiswa:[]
        },
        methods: {
            addMhs:function(){
                this.$http.post('/api/mahasiswa', {nim: this.nim, nama: this.nama, jenis_kelamin: this.jenis_kelamin}).then(response => {
                // console.log(response.body.id);
                alert('Berhasil menambahkan data');
                this.mahasiswa.unshift({'id':response.body.id, 'nim':this.nim, 'nama':this.nama, 'jenis_kelamin':this.jenis_kelamin, 'status_kehadiran':this.status_kehadiran}); //menambahkan properti id sehingga data yang baru saja di tambahkan bisa langsung di update/delete tanpa di reload dahulu
                //this.id=0;
                this.nim=0;
                this.nama='';
                this.jenis_kelamin='';
                // this.getData();

                });
            },
            deleteMhs:function(index,mhs){
                let cek = confirm('Yakin ingin Menghapus data?');
                if(cek){
                this.$http.delete('/api/mahasiswa/'+mhs.id+'/delete').then(response => {
                alert('Berhasil menghapus data');
                this.mahasiswa.splice(index,1);
                // this.getData();

                });
            }
            },
            cekHadir:function(index,mhs){
                // console.log(JSON.parse(JSON.stringify(mhs)));
                this.$http.patch('/api/mahasiswa/'+mhs.id+'/hadir').then(response => {
                    alert('Berhasil merubah data');
                if(mhs.status_kehadiran){
                    this.mahasiswa[index].status_kehadiran=0;
                }else{
                    this.mahasiswa[index].status_kehadiran=1;
                }

                });
            },
            getData:function(){
                this.$http.get('/api/mahasiswa').then(response => {
                //  console.log(response.body.data);
                 this.mahasiswa = response.body.data;
            });
            }
        },
        mounted:function(){
            this.getData();
        }
    })
</script>
</body>
</html>