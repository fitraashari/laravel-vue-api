<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MahasiswaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nim'=>$this->nim,
            'nama'=>$this->nama,
            'jenis_kelamin'=>$this->jenis_kelamin,
            'status_kehadiran'=>$this->status_kehadiran
        ];
    }
}
