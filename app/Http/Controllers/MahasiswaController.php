<?php

namespace App\Http\Controllers;

use App\Http\Resources\MahasiswaResource;
use App\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    //
    public function index(){
        $mahasiswa = Mahasiswa::orderBy('created_at','desc')->get();
        return MahasiswaResource::collection($mahasiswa);
    }
    public function create(Request $request){
        // request()->validate([
        //     'nim'=>['required'],
        //     'nama'=>['required'],
        //     'jenis_kelamin'=>['required']
        // ]);
        $mahasiswa = Mahasiswa::create([
            'nim'=>$request['nim'],
            'nama'=>$request['nama'],
            'jenis_kelamin'=>$request['jenis_kelamin'],
            'status_kehadiran'=>0,
        ]);
        return $mahasiswa;
    }
    public function destroy($id){
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->delete();
        return 'success';
    }
    public function statusHadir($id){
        $mahasiswa=Mahasiswa::find($id);
        if($mahasiswa->status_kehadiran==0){
            $hadir=1;
        }else{
            $hadir=0;
        }
        $mahasiswa->update([
            'status_kehadiran'=>$hadir
        ]);
        return $mahasiswa;
    }
}
